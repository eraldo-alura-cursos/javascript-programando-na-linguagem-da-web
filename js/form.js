var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function () {
    event.preventDefault();
    var form = document.querySelector("#form-adiciona");

    var paciente = recuperaInformacoesNovoPacienteForm(form);

    var erro = validaPaciente(paciente);

    if(erro.length > 0) {
        exibeMensagensDeErro(erro);
        return;
    }

    form.reset();
    var ul = document.querySelector("#mensagens-erro");
    ul.innerHTML = "";
});

function recuperaInformacoesNovoPacienteForm(form) {

    var paciente = {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    };

    return paciente;
}

function adicionaPacienteNaTabela(paciente) {
    var trPaciente = montarTrHtml(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(trPaciente);
}

function montarTrHtml(paciente) {

    var pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");

    pacienteTr.appendChild(montarTdHtml(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montarTdHtml(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montarTdHtml(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montarTdHtml(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montarTdHtml(paciente.imc, "info-imc"));

    return pacienteTr;
}

function montarTdHtml(dado, classe) {
    var td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);

    return td;
}

function validaPaciente (paciente) {

var erros = [];

    if (paciente.nome.length == 0) {
        erros.push("O nome não pode ser em branco");
    }

    if (!validaPeso(paciente.peso) || paciente.peso.length == 0) {
        erros.push("Peso é inválido");
    }

    if (!validaAltura(paciente.altura) || paciente.altura.length == 0) {
        erros.push("Altura é inválida!");
    }

    if (paciente.gordura.length == 0) {
        erros.push("Gordura não pode ser em branco")
    }

    return erros
}


function exibeMensagensDeErro(erros) {
    var ul = document.querySelector("#mensagens-erro");
    ul.innerHTML = "";
    erros.forEach(erro => {
        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);
    });
}