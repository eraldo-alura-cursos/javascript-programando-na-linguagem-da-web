var titulo = document.querySelector(".titulo");
var pacientes = document.querySelectorAll(".paciente");
titulo.textContent = "Aparecida Nutricionista";

pacientes.forEach(paciente => {

    var tdPeso = paciente.querySelector(".info-peso");
    var tdAltura = paciente.querySelector(".info-altura");
    var tdImc = paciente.querySelector(".info-imc");

    var peso = tdPeso.textContent;
    var altura = tdAltura.textContent;

    var pesoEhValido = validaPeso(peso);
    var alturaEhValida = validaAltura(altura);

    if (!pesoEhValido) {
        console.log("Peso inválido!");
        paciente.classList.add("paciente-invalido");
        tdImc.textContent = "Peso inválido!"
    }
    if (!alturaEhValida) {
        console.log("Altura inválida!");
        paciente.classList.add("paciente-invalido");
        tdImc.textContent = "Altura inválida!"
    }

    if (alturaEhValida && pesoEhValido) {

        var imc = calculaImc(peso, altura);
        tdImc.textContent = imc;
    } else {
        
        
    }
});

function validaPeso(peso) {
    if (peso >= 0 && peso< 1000) {
        return true;
    } else {
        return false;
    }
}

function validaAltura(altura) {
    if (altura >= 0 && altura< 3.0) {
        return true;
    } else {
        return false;
    }
}

function calculaImc(peso, altura){
    var imc = 0;
    imc = peso / Math.pow(altura, 2);

    return imc.toFixed(2);
}