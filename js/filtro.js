var inputFiltroNome = document.querySelector("#filtrar-tabela");

inputFiltroNome.addEventListener("input", function(){
    this.value;
    var pacientes = document.querySelectorAll(".paciente");

    if(this.value.length > 0){
        pacientes.forEach(paciente => {
            var tdNome = paciente.querySelector(".info-nome");
            var nome = tdNome.textContent;
            var regexp = new RegExp(this.value, "i");
            if (!regexp.test(nome)) {
                paciente.classList.add("display-none");
            } else {
                paciente.classList.remove("display-none");
            }
        });
    } else {
        pacientes.forEach(paciente => {
            paciente.classList.remove("display-none");
        });
    }
});